# PicoScale Readout README file

Reads output file of the PicoScale readout that can be exported from the phantom software.

Plots the sensor readout in four different ways (Lumerical-interface-like):
- Top view (x-y)
- Side view (x-z)
- Side view (y-z)
- 3d view (x-y-z)


# Input instructions

Input should be as follows:
python read_pico.py file_name.bin

where file_name.bin is the file exported with phantom software, with the entire path.