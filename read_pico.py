# -*- coding: utf-8 -*-
"""
Created on Mon Feb 06 18:33:31 2017

@author: ldeangelis
"""

import numpy as np
import matplotlib.pyplot as plt
import sys
import subprocess
import struct
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def plot_position(x,y,z):
    # Given x,y,z plots four panels with the three 2D views and the 3D view
    
    fig = plt.figure(figsize = (10,10))
    ax = fig.add_subplot(2, 2, 1)
    ax.plot(x,y)
    ax.set_title('X-Y Top View')
    ax.set_xlabel(r'$x\,(\mu m)$')
    ax.set_ylabel(r'$y\,(\mu m)$')
    ax = fig.add_subplot(2, 2, 3)
    ax.plot(x,z)
    ax.set_title('X-Z Side View')
    ax.set_xlabel(r'$x\,(\mu m)$')
    ax.set_ylabel(r'$z\,(\mu m)$')
    ax = fig.add_subplot(2, 2, 4)
    ax.plot(y,z)
    ax.set_xlabel(r'$y\,(\mu m)$')
    ax.set_ylabel(r'$z\,(\mu m)$')  
    ax.set_title('Z-Y Side View')
    ax = fig.add_subplot(2, 2, 2, projection = '3d')
    ax.plot(x,y,z)
    ax.set_xlabel(r'$x\,(\mu m)$')
    ax.set_ylabel(r'$y\,(\mu m)$')  
    ax.set_zlabel(r'$z\,(\mu m)$')
    ax.set_title('3D Plot')
        
    plt.show()
    
    return
    
    
    
    
if __name__ == '__main__':

    # Name of the input file (with path)
    fname = sys.argv[1]
    
    # Git version of the code    
    git_v = subprocess.check_output(["git", "rev-parse", "--verify", "--short", "HEAD"])
    
    
    # Define the structure of the data
    dt = np.dtype([('x', 'int64'),('y', 'int64'),('z', 'int64')])
    
    with open(fname,'rb') as fin:
        # Read the header
        frequency, = struct.unpack("i",fin.read(4))
        aggregation, = struct.unpack("i",fin.read(4))
        channels, = struct.unpack("i",fin.read(4))
        dummy, = struct.unpack("i",fin.read(4))
        # And the data content
        A = np.fromfile(fin, dtype = dt)
            
    x = A['x']/1000000.0 # Save and convert everything to micron
    y = A['y']/1000000.0
    z = A['z']/1000000.0
    del A
    
    print '%d data points successfully imported.' % (len(x))
    

    plot_position(x,y,z)
